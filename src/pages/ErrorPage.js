import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ErrorPage(){
  return(
    <Row>
      <Col className = "p-5 text-center">
        <h4>Zuitt Booking</h4>
        <h1>Page Not Found</h1>
        <p>Go back to the <Link to="/">homepage</Link>.</p>
      </Col>
    </Row>
  )
}