import { Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';

export default function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  console.log(email);
  console.log(password);

  function Login(e) {
    e.preventDefault();

    //set the email of the authenmticated user in the local storage
    //syntax:localStorage.setItem('propertyNamme', property)
    localStorage.setItem('email', email);

    setEmail('');
    setPassword('');

    alert('You are now logged in!');
  }

  useEffect(() => {
    if (email && password) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  function refreshPage() {
    window.location.reload();
  }

  return (
    <Form onSubmit={(e) => Login(e)}>
      <br/>
      <h3>Login</h3>
      <Form.Group controlId="userEmail">
        <Form.Label>Email Address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          required
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </Form.Group>

      <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          required
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </Form.Group>

      {isActive ? (
        <>
          <br />
          <Button variant="success" type="submit" id="submitBtn" onClick ={refreshPage}>
            Submit
          </Button>
        </>
      ) : (
        <>
          <br />
          <Button variant="success" type="submit" id="submitBtn">
            Submit
          </Button>
        </>
      )}
    </Form>
  );
}
