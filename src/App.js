/*import { Fragment } from 'react';*/
import { Container } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ErrorPage from './pages/ErrorPage';
import { BrowserRouter as Router, Routes, Route, Link} from 'react-router-dom';
import './App.css';




function App() {
  return (
//In react JS, multiple components rendered in a single component should be wrapped in a parent components
//"Fragment" ensures that an error will be prevented.
//you can use <></> replacing fragment
    <Router>
        <AppNavbar/>
{/*            <Home/>*/}
        <Container>
            <Routes>
                <Route path = "/" element = {<Home/>} />
                <Route path = "/Courses" element = {<Courses/>} />
                <Route path = "/Login" element = {<Login/>} />
                <Route path = "/Register" element = {<Register/>} />
                <Route path = "/logout" element = {<Logout/>} />
                <Route path="*" element={<ErrorPage />} />
            </Routes>
        </Container>

    </Router>

  );
}

export default App;
   