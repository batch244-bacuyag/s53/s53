import {useState} from 'react';
import { Navbar, Container, Nav} from 'react-bootstrap';
import { Link, NavLink} from 'react-router-dom';
import ErrorPage from '../pages/ErrorPage';


export default function AppNavbar({isErrorPage}){

  //state to store the user information stored in the loin page
  const [user, setUser] = useState(localStorage.getItem('email'));
  console.log(user);

	return(
    <Navbar bg="primary" expand="lg">
      <Container>
        <Navbar.Brand as ={NavLink} to="/">Zuitt Booking</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as ={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
            
            {(user !== null)

              ?
              <Nav.Link as ={NavLink} to="/logout">Logout</Nav.Link>

              :
              <>
              <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
              <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
              </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
	)
};

//export default AppNavbar(can also be used)