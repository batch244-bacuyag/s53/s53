import { Button, Row, Col } from 'react-bootstrap';
import ErrorPage from '../pages/ErrorPage';

export default function Banner({isErrorPage}){

	if(isErrorPage){
		return <ErrorPage/>;
	} else{
		return(
		<>	
		<Row>
			<Col className = "p-5 text-center">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everone, everwhere.</p>
				<Button variant = "primary">Enroll Now!</Button>
			</Col>
		</Row>	
		</>	
		)
	}
}